import React, {Component} from 'react'
import Title from './Title';

class AddPhoto extends Component {
    constructor() {
        super()
        this.handelSubmit= this.handelSubmit.bind(this);
    }
    handelSubmit(event) {
        event.preventDefault();
        console.log("submit",event.target.elements);
        const imageLink = event.target.elements.link.value;
        const description = event.target.elements.description.value;
        const post = {
            id: new Date(),
            description: description,
            imageLink:imageLink
        }
        if(imageLink && description){
            this.props.onAddPhoto(post)
        }
    }
    render() {
        return (
        <div>
            <Title title = {'Photowall'}/> 
            <div className="form">
                <form onSubmit={this.handelSubmit}>
                    <input type="text" placeholder="Link" name="link"/>
                    <input type="text" placeholder="Description" name="description"/>
                    <button>post</button>
                </form>
            </div>
        </div>
        )
    }
}

export default AddPhoto;